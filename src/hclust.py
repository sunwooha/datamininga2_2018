#A template for the implementation of hclust
import math #sqrt
import csv
import sys

# Accepts two data points a and b.
# Returns the distance between a and b.
# Note that this might be specific to your data.
def Distance(a,b):
    sum = 0
    if(len(a) == len(b)):
        for num in range(len(a)):
            sum += (a[num] - b[num])**2
    return math.sqrt(sum)

# Accepts two data points a and b.
# Produces a point that is the average of a and b.
def merge(a,b):
    average_ab = []
    if(len(a) == len(b)):
        for num in range (len(a)):
            avg_feat = (a[num] + b[num])/2
            average_ab.append(avg_feat)
    return average_ab

# Accepts a list of data points.
# Returns the pair of points that are closest
def findClosestPair(D):
    min_dist = Distance(D[0], D[1])
    closest_locations = [D[0], D[1]]
    for center in D:
        other_D = D.copy()
        other_D.remove(center)
        for other_center in other_D:
            current_dist = Distance(center, other_center)
            if current_dist < min_dist:
                min_dist = current_dist
                closest_locations = [center, other_center]
    return closest_locations

# Accepts a list of data points.
# Produces a tree structure corresponding to a
# Agglomerative Hierarchal clustering of D.
def HClust(D):
    centers = D.copy()
    splits = dict()
    while len(centers) > 1:
        location = findClosestPair(centers)
        for l in location:
            centers.remove(l)
        merge_location = merge(location[0], location[1])
        centers.append(merge_location)
        splits[tuple(merge_location)] = location
    return splits

def main():
    D = []
    with open(sys.argv[1]) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        count = 0
        for row in reader:
            if count != 0:
                r = []
                for feature in row:
                    r.append(float(feature))
                D.append(r)
            count += 1

    splits = HClust(D)
    for s in splits:
        print(s, ":", splits[s])
    print("k =", len(splits))

if __name__ == "__main__":
    main()