#A template for the implementation of K-Medoids.
import math #sqrt
import csv
import sys

# Accepts two data points a and b.
# Returns the distance between a and b.
# Note that this might be specific to your data.
def Distance(a,b):
    sum = 0
    if(len(a) == len(b)):
        for num in range(len(a)):
            sum += abs(a[num] - b[num])
    return sum

# Accepts a list of data points D, and a list of centers
# Returns a dictionary of lists called "clusters", such that
# clusters[c] is a list of every data point in D that is closest
#  to center c.
# Note: This can be identical to the K-Means function of the same name.
def assignClusters(D, centers):
    clusters = dict()
    for point in D:
        nearest = centers[0]
        for c in centers:
            if (Distance(c, point) < Distance(nearest, point)):
                nearest = c
        clusters.setdefault(tuple(nearest),[]).append(point)
    return clusters

# Accepts a list of data points.
# Returns the medoid of the points.
def findClusterMedoid(c, cluster):
    for cand in cluster:
        dist_c = 0
        dist_cand = 0
        for point in cluster:
            dist_c += Distance(c, point)
            dist_cand += Distance(cand, point)
        if dist_cand < dist_c:
            c = cand
    return c

# Accepts a list of data points, and a number of clusters.
# Produces a set of lists representing a K-Medoids clustering
#  of D.
def KMedoids(D, k):
    centers = D[0:k]
    oldCenters = []
    clusters = dict()
    while centers != oldCenters:
        oldCenters = []
        for i in range (len(centers)):
            oldCenters.append(centers[i])
        clusters = assignClusters(D, centers)
        for c in centers:
            centers[centers.index(c)] = findClusterMedoid(c, clusters.get(tuple(c)))
    return clusters

def main():
    D = []
    k = int(sys.argv[2])

    with open(sys.argv[1]) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        count = 0
        for row in reader:
            if count != 0:
                r = []
                for feature in row:
                    r.append(float(feature))
                D.append(r)
            count += 1

    clusters = KMedoids(D, k)
    for c in clusters:
        #print(c, ":", clusters[c])
        print(c)

if __name__ == "__main__":
    main()
