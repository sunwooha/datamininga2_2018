#A template for the implementation of K-Means.
import math #sqrt
import csv
import sys

# Accepts two data points a and b.
# Returns the euclidian distance
# between a and b.
def euclidianDistance(a,b):
    sum = 0
    if(len(a) == len(b)):
        for num in range(len(a)):
            sum += (a[num] - b[num])**2
    return math.sqrt(sum)

# Accepts a list of data points D, and a list of centers
# Returns a dictionary of lists called "clusters", such that
# clusters[c] is a list of every data point in D that is closest
#  to center c.
def assignClusters(D, centers):
    clusters = dict()
    for point in D:
        nearest = centers[0]
        for c in centers:
            if (euclidianDistance(c, point) < euclidianDistance(nearest, point)):
                nearest = c
        clusters.setdefault(tuple(nearest),[]).append(point)
    return clusters

# Accepts a list of data points.
# Returns the mean of the points.
def findClusterMean(cluster):
    mean_point = []
    for i in range(len(cluster[0])):
        feature_sum = 0
        for point in cluster:
            feature_sum += point[i]
        feature_mean = feature_sum/len(cluster)
        mean_point.append(feature_mean)
    return mean_point

# Accepts a list of data points, and a number of clusters.
# Produces a set of lists representing a K-Means clustering
#  of D.
def KMeans(D, k):
    means = D[0:k]
    oldMeans = []
    clusters = dict()
    while means != oldMeans:
        oldMeans = []
        for i in range (len(means)):
            oldMeans.append(means[i])
        clusters = assignClusters(D, means)
        for m in means:
            means[means.index(m)] = findClusterMean(clusters.get(tuple(m)))
    return clusters

def main():
    D = []
    k = int(sys.argv[2])

    with open(sys.argv[1]) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        count = 0
        for row in reader:
            if count != 0:
                r = []
                for feature in row:
                    r.append(float(feature))
                D.append(r)
            count += 1
            
    clusters = KMeans(D, k)
    for c in clusters:
        #print(c, ":", clusters[c])
        print(c)

if __name__ == "__main__":
    main()
