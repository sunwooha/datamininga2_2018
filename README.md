# Assignment 2: Implementation of K-Means, K-Medoids, and Bottom-Up Hierarchical Clustering

For this assignment, we implemented K-Means, K-Medoids, and Bottom-Up Hierarchical Clustering. We explored the clusters obtained  from these algorithms using the data from the study, A Theory of Extramarital Affairs.
In 'report.pdf', we discuss the groupings found from each algorithm. We also created visualizations for the 3 algorithms using R. 

How to Run the Program
----------------------
- Open the terminal.
- Go the the directory that holds the  source files. We recommend using k = 2 on these algorithms.
- To run analysis on the affairs dataset using K-Means:
        > python3 kmeans.py ../data/affairs_2.csv <number of k>
- To run analysis on the affairs dataset using K-Medoids:
       > python3 kmedoids.py ../data/affairs_2.csv <number of k>
- To run analysis on the affairs dataset using HClust (the algorithm is very slow...will take several hours):
      > python3 hclust.py ../data/affairs_2.csv 
